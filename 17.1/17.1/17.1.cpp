﻿#include <iostream>


class Hire
{
private:
    int b;
    void ShowA();

public:
    int GetB()
    {
        return b;
    }
    void SetB(int NewB)
    {
        b = NewB;
     
    }


};

class Vector
{
public: 
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void ShowModule()
    {
        VectorModule = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << "Module of vector:" << VectorModule << std::endl;
    }
private:
    double x;
    double y;
    double z;
    double VectorModule;
};


int main()
{
    Hire temp;
    temp.SetB(10);
    std::cout << temp.GetB();
    Vector v; 
    v.ShowModule();
    
}


